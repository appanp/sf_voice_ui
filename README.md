
[violet](https://helloviolet.ai/) provides support for building sophisticated conversational apps/bots on Amazon's Alexa and Google Home. Conversations are built via scripts, and Violet provides a conversation engine that runs as an Alexa Skill. Conversational flows are defined using XML-based language](https://helloviolet.ai/docs/conversation-flow-design) which has [I/O Elements](https://helloviolet.ai/docs/conversation-input-output), [Conversational Elements](https://helloviolet.ai/docs/conversation-elements) and [Application Logic Elements](https://helloviolet.ai/docs/conversation-app-logic).

This [Quickstart Violet](https://trailhead.salesforce.com/content/learn/projects/quickstart-violet) trailhead modules provides a quick-start project for Violet which runs as a node app in Heroku.

Other learning resources for developing Voice UI (VUI) on Salesforce platform are listed below:

1. [Innovate with Alexa and AWS](https://trailhead.salesforce.com/en/content/learn/trails/innovate-with-alexa-and-amazon-web-services), Trailhead trail
1. [Build an iOS app that uses chatbots](https://trailhead.salesforce.com/content/learn/projects/ios-app-chatbot)
1. [Build an Einstein bot](https://trailhead.salesforce.com/en/content/learn/projects/build-an-einstein-bot?trail_id=service_einstein)
1. [Open-Sourcing Violet - A voice application framework](https://engineering.salesforce.com/open-sourcing-violet-a-voice-application-framework-744f7c660655)
1. [Violet github repo](https://github.com/salesforce/violet-conversations)

